import os
import random
import time

from blessed import Terminal


TERM = Terminal()
HEIGHT = Terminal().height
WIDTH = Terminal().width
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
WORDLIST_FP = os.path.join(PROJECT_DIR, 'words.txt')

# Codes for blessed to recognize user input
BACKSPACE = [263, 330]
ENTER = 343


def load_words(word_file: str = WORDLIST_FP):
    """ Return a list of random words from word list. """

    with open(word_file, 'r') as f:
        return [word.rstrip() for word in f]


def make_word_lines(all_words: list[str], lines_to_generate: int = 5):
    """ Return a list of random words under width length. """

    lines = []
    type_width = min(76, WIDTH - 4)  # -2 for user area, -2 for overlaps
    last_word = r_word = ''

    for line in range(lines_to_generate):
        words_in_line = []
        while (len(' '.join(words_in_line)) + len(r_word)) < type_width:
            r_word = random.choice(all_words)
            if r_word != last_word:
                words_in_line.append(r_word)
                last_word = r_word
        lines.append(words_in_line)

    return lines


def get_time():
    """ Return current time in seconds. """

    return int(time.time())


def type_screen(
    completed_words: list[str],
    current_line: list[str],
    next_word_line: str,
    user_input: str
):
    """ Display current typing session. """

    print(TERM.clear)

    current_words_row = TERM.move_y(HEIGHT // 2 - 1)  # Above the center
    next_words_row = TERM.move_y(HEIGHT // 2)         # Center of the screen
    user_input_row = TERM.move_y(HEIGHT // 2 + 2)     # Below the center

    completed_words_str = ''.join([f'{w} ' for w in completed_words])
    print(current_words_row + ' ' + completed_words_str +
          ' '.join(current_line))
    print(next_words_row + TERM.gray(' ' + ' '.join(next_word_line)))
    print(user_input_row + TERM.bright_white('>') + ' ' + user_input +
          TERM.bright_white('_'))


def game_over(counts: dict, times: dict):
    """ Display end screen with WPM and accuracy. """

    print(TERM.clear)

    minutes = (times['end'] - times['start']) / 60
    wpm = max(int((counts['chars'] / 5 - counts['word_errors']) // minutes), 0)
    accuracy = round(100 * (counts['chars'] - counts['errors']) / counts['chars'], 2)

    print(
        TERM.move_y(0) +
        f'''Your WPM was {wpm}.\nYour accuracy was {accuracy}%.'''
    )


if __name__ == '__main__':
    all_words = load_words(WORDLIST_FP)

    word_lines = make_word_lines(all_words)
    chars = 0
    errors = 0
    word_errors = 0
    game_started = False

    with TERM.hidden_cursor():
        with TERM.cbreak():
            while len(word_lines) > 0:
                current_line = word_lines.pop(0)
                next_line = word_lines[0] if len(word_lines) > 0 else ''
                completed_words = []
                user_input = ''
                curr_char_index = 0
                curr_word = current_line[0]

                while len(current_line) > 0:
                    type_screen(
                        completed_words,
                        current_line,
                        next_line,
                        user_input
                    )
                    val = TERM.inkey()
                    chars += 1

                    # Get start time as first key is pressed
                    if not game_started:
                        start_time = get_time()
                        game_started = True

                    # If backspace, delete last input
                    if val.code in BACKSPACE:
                        user_input = user_input[:-1]
                        curr_char_index -= 1
                    elif val == ' ' or val.code == ENTER:
                        # Don't let a double space cause a failed word
                        if user_input == '':
                            continue

                        if user_input == curr_word:
                            color = TERM.green
                        else:
                            color = TERM.red
                            word_errors += 1

                        completed_words.append(color + curr_word + TERM.normal)
                        user_input = ''
                        curr_char_index = 0

                        # Go to next word
                        current_line.pop(0)
                        if len(current_line) > 0:
                            curr_word = current_line[0]
                        else:
                            curr_word = None

                    # Otherwise if not a backspace nor a space,
                    else:
                        user_input += val

                        # If incorrect character, add error
                        if (curr_char_index >= len(curr_word) or
                                val != curr_word[curr_char_index]):
                            errors += 1

                        curr_char_index += 1

    # When all lines have been typed, start game over sequence
    end_time = get_time()

    counts = {
        'chars': chars,
        'errors': errors,
        'word_errors': word_errors,
    }
    times = {
        'start': start_time,
        'end': end_time,
    }

    game_over(counts, times)
